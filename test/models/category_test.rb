require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  def setup
    @category = Category.new(name: "Entretenimiento")
  end

  test "la categoría debería ser válida" do
    assert @category.valid?
  end

  test "el nombre debería estar presente" do
    @category.name = " "
    assert_not @category.valid?
  end

  test "el nombre debería ser único" do
    @category.save
    @category2 = Category.new(name: "Entretenimiento")
    assert_not @category2.valid?
  end

  test "el nombre no debería ser demasiado largo" do
    @category.name = "a" * 26
    assert_not @category.valid?
  end

  test "el nombre no debería ser demasiado corto" do
    @category.name = "aa"
    assert_not @category.valid?
  end
end 