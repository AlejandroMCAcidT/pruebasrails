require 'test_helper'
class ListCategoriesTest < ActionDispatch::IntegrationTest
    
    def setup
        @category = Category.create(name: "Deportes")
        @category2 = Category.create(name: "Programación")
    end

    test "debería mostrar el lístado de categorías" do
        get categories_path
        assert_template 'categories/index'
        assert_select "a[href=/?]", categories_path(@category), text: @category.name
        assert_select "a[href=/?]", categories_path(@category2), text: @category2.name
    end
end