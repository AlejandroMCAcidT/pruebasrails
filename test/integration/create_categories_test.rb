require 'test_helper'

class CreateCategoryTest < ActionDispatch::IntegrationTest

    def setup
        @category = Category.create(name: "Sports")
        @user = User.create(username: "john", email: "john@example.com", password: "password", admin: true)
    end

    test "should get categories index" do
        get categories_path
        assert_response :success
    end
      
    test "should get new" do
        sign_in_as(@user, "password")
        get new_category_path
        assert_response :success
    end
      
    test "should get show" do
        get categories_path(@category)
        assert_response :success
    end
      
    test "should redirect create when admin not logged in" do
        assert_no_difference 'Category.count' do
          post categories_path, params: { category: { name: "Entretenimiento" } }
        end
        assert_redirected_to categories_path
    end

    test "recoger formulario de nueva categoría y crear nueva" do
        sign_in_as(@user, 'password')
        get new_category_path
        assert_template 'categories/new'
        assert_difference 'Category.count',1 do
            post categories_path, params: {category: {name: "Entretenimiento"}}
            follow_redirect!
        end
        assert_template 'categories/index'
        assert_match 'Entretenimiento',response.body
    end
  
    test "introducir categoría inválida acaba en fallo" do
        sign_in_as(@user, 'password')
        get new_category_path
        assert_template 'categories/new'
        assert_no_difference 'Category.count' do
            post categories_path, params: {category: {name: " "}}
        end
        assert_template 'categories/new'
        assert_select 'h4.alert-heading'
    end

end 