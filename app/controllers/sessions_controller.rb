class SessionsController < ApplicationController

    def new
    end
  
    def create
        user = User.find_by(email: params[:session][:email].downcase)
        if user && user.authenticate(params[:session][:password])
            session[:user_id] = user.id
            flash[:notice] = 'Se ha iniciado sesión correctamente'
            redirect_to user
        else
            flash.now[:alert] = 'Ha ocurrido un error con los datos introducidos. Compruebe su email y su contraseña.'
            render 'new'
        end
    end
  
    def destroy
        session[:user_id] = nil
        flash[:notice] = 'Se ha cerrado la sesión'
        redirect_to root_path
    end
  
end 